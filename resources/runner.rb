#
# Cookbook:: gitlab-ci-runner
# Resource:: runner
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true

include GitlabCiRunner::Cookbook::Helpers
include GitlabCiRunner::Cookbook::RunnerHelpers
include GitlabCiRunner::Cookbook::TomlHelpers

property :config_file, String,
          default: lazy { default_gitlab_runner_config_file },
          desired_state: false,
          description: 'Configuration file location'

property :cookbook, String,
          default: 'gitlab-ci-runner',
          desired_state: false,
          description: 'Cookbook to source configuration file template from'

property :template, String,
          default: 'gitlab-runner/config.toml.erb',
          desired_state: false,
          description: 'Template to use to generate the configuration file'

property :owner, String,
          default: lazy { default_gitlab_runner_user },
          description: 'Owner of the generated configuration file'

property :group, String,
          default: lazy { default_gitlab_runner_group },
          description: 'Group of the generated configuration file'

property :mode, String,
          default: '0600',
          description: 'Filemode of the generated configuration file'

property :sensitive, [true, false],
          desired_state: false,
          default: true,
          description: 'Enable sensitive by default'

property :url, String,
          desired_state: false,
          required: true,
          description: 'Gitlab Server URL'

property :registration_token, String,
          desired_state: false,
          sensitive: true,
          description: 'Legacy server runner registation token',
          deprecated: 'The legacy workflow that uses registration tokens is deprecated and will be removed in GitLab 18.0. Migrate to authentication tokens.',
          callbacks: {
            'is incorrect, gitlab runner registration tokens do not start with `glrt`' => lambda { |p|
              !p.start_with?(GitlabCiRunner::Cookbook::RunnerHelpers::RUNNER_TOKEN_PREFIX)
            },
          }

property :token, String,
          sensitive: true,
          description: 'Runner authentication token',
          callbacks: {
            'is incorrect, gitlab runner instance tokens start with `glrt`' => lambda { |p|
              (p.start_with?(GitlabCiRunner::Cookbook::RunnerHelpers::RUNNER_TOKEN_PREFIX) || !p.include?('-'))
            },
          }

deprecated_property_alias 'authentication_token', 'token', 'Deprecated: Use then token property for pre-created runners.'

property :administration_token, String,
          desired_state: false,
          sensitive: true,
          description: 'Server user administration token for updating runner server-side options'

property :http_options, Hash,
          default: lazy { default_gitlab_runner_http_options },
          desired_state: false,
          coerce: proc { |p| p.merge(default_gitlab_runner_http_options) },
          description: 'Set net/http options for API requests'

property :description, String,
          default: lazy { name },
          description: 'Server - Runner description'

property :paused, [true, false],
          default: false,
          description: 'Server - Runner paused flag'

deprecated_property_alias 'active', 'paused', 'Deprecated: Use paused instead. Specifies if the runner is allowed to receive new jobs'

property :tag_list, Array,
          default: [],
          description: 'Server - Runner tag list'

property :run_untagged, [true, false],
          description: 'Server - Runner run untagged option'

property :locked, [true, false],
          description: 'Server - Runner locked option'

property :access_level, [String, Symbol],
          equal_to: ['not_protected', 'ref_protected', :not_protected, :ref_protected],
          coerce: proc { |p| p.to_s },
          description: 'Server - Runner access level'

property :runner_type, [String, Symbol],
          default: :instance_type,
          desired_state: false,
          equal_to: ['instance_type', 'group_type', 'project_type', :instance_type, :group_type, :project_type],
          coerce: proc { |p| p.to_s },
          description: 'Server - Runner scope type'

property :group_id, Integer,
          desired_state: false,
          description: 'The ID of the group that the runner is created in. Required if runner_type is group_type.'

property :project_id, Integer,
          desired_state: false,
          description: 'The ID of the project that the runner is created in. Required if runner_type is group_type.'

property :maximum_timeout, Integer,
          description: 'Server - Runner maximum timeout value'

property :maintenance_note, String,
          callbacks: {
            'maintenance_note must be 1024 characters or less' => ->(p) { p.length <= 1024 },
          },
          description: 'Server - Free-form maintenance notes for the runner (1024 characters)'

property :options, Hash,
          default: {},
          coerce: proc { |p| p.transform_keys(&:to_s) },
          description: 'Local - Runner options'

load_current_value do |new_resource|
  current_value_does_not_exist! unless ::File.exist?(new_resource.config_file) && runner_config_present?(new_resource.name, new_resource.config_file)

  if ::File.exist?(new_resource.config_file)
    owner ::Etc.getpwuid(::File.stat(new_resource.config_file).uid).name
    group ::Etc.getgrgid(::File.stat(new_resource.config_file).gid).name
    mode ::File.stat(new_resource.config_file).mode.to_s(8)[-4..-1]
  end

  if property_is_set?(:administration_token)
    server_config_state = runner_server_config(new_resource.name, new_resource.config_file, new_resource.administration_token, new_resource.http_options)
    runner_server_properties.each { |p| send(p, server_config_state.fetch(p.to_s, nil)) }
  end

  token runner_local_config(new_resource.name, new_resource.config_file).fetch('token')
  options runner_local_config(new_resource.name, new_resource.config_file).reject { |k, _| %w(name url token).include?(k) }
end

action_class do
  include GitlabCiRunner::Cookbook::ResourceHelpers
  include GitlabCiRunner::Cookbook::RunnerHelpers

  include GitlabCiRunner::Cookbook::CommonHelpers

  def server_setting(option)
    return new_resource.send(option) unless new_resource.send(option).nil?
    return new_resource.options.fetch(option.to_s, nil) unless new_resource.options.fetch(option.to_s, nil)

    nil
  end

  def server_runner_options
    options = runner_server_properties.map { |p| [p.to_s, server_setting(p).dup] }.to_h
    compact_hash!(options)

    options
  end

  def local_runner_options
    local = GitlabCiRunner::Cookbook::RunnerHelpers::RUNNER_LOCAL_OPTIONS.map { |p| [p.to_s, new_resource.send(p).dup] }.to_h
    local.merge!(runner_options_to_h(new_resource.options.dup))
    local.delete_if { |option, _| runner_server_properties.include?(option) }

    local
  end

  def validate_options
    raise Chef::Exceptions::ValidationFailed, 'Keys of the Hash provided to the options property MUST be of type String' unless hash_keys_string?(new_resource.options)

    case runner_registration_type
    when :register
      raise Chef::Exceptions::ValidationFailed, 'registration_token is a required property' unless property_is_set?(:registration_token)
    when :create
      %i(administration_token runner_type).each do |p|
        raise Chef::Exceptions::ValidationFailed, "#{p} is a required property" if nil_or_empty?(new_resource.send(p))
      end

      raise Chef::Exceptions::ValidationFailed, 'group_id is a required property' if new_resource.runner_type.eql?('group_type') && !property_is_set?(:group_id)
      raise Chef::Exceptions::ValidationFailed, 'project_id is a required property' if new_resource.runner_type.eql?('project_type') && !property_is_set?(:project_id)
    when :supplied_token
      raise Chef::Exceptions::ValidationFailed, 'token is a required property' unless property_is_set?(:token)
    end
  end
end

action :create do
  run_action(:register)
end

action :register do
  validate_options
  config_resource_init

  return if runner_registered?(new_resource.name, new_resource.url, new_resource.http_options)

  id, token = nil

  case runner_registration_type
  when :supplied_token
    raise Chef::Exceptions::ValidationFailed, 'Bad auth token!' unless api_runner_token_valid?(new_resource.url, new_resource.http_options, new_resource.token)

    # Get runner ID from Gitlab server for future server-side configuration updates
    converge_by("Get runner ID for gitlab-runner #{new_resource.name} from server #{new_resource.url} with authentication token.") do
      id = runner_server_token_id
      token = new_resource.token.dup
    end
  when :create
    # Create with Gitlab server
    converge_by("Create gitlab-runner #{new_resource.name} on server #{new_resource.url}.") do
      id, token = runner_create(new_resource.url, new_resource.http_options, new_resource.administration_token, server_runner_options)
      log "Runner #{new_resource.name} created on server #{new_resource.url} with ID: #{id}."
    end
  when :register
    # Register with Gitlab server
    converge_by("Register gitlab-runner #{new_resource.name} with server #{new_resource.url}.") do
      id, token = runner_register(new_resource.url, new_resource.http_options, new_resource.registration_token, server_runner_options)
      log "Runner #{new_resource.name} registered with #{new_resource.url} as ID: #{id}."
    end
  end

  # Local configuration
  converge_by("Add gitlab runner #{new_resource.name} to local configuration.") do
    new_runner = local_runner_options.dup
    new_runner['name'] = "#{id}: #{new_resource.name}"
    new_runner['token'] = token

    new_runner = compact_hash!(new_runner)
    log_chef(:info, "Adding new runner #{new_runner['name']} with config #{new_runner} to configuration file #{new_resource.config_file}.")
    config_resource_runners.push(new_runner)
  end
end

action :update do
  validate_options
  config_resource_init

  log_chef(
    :warn,
    "\n     Unable to check/update server options for Runner: #{new_resource.name} as an administration token has not been specified. Previous values may be incorrect."
  ) unless property_is_set?(:administration_token)

  raise "Unable to update unregistered runner #{new_resource.name} for url #{new_resource.url}." unless runner_registered?(new_resource.name, new_resource.url, new_resource.http_options)

  converge_if_changed do
    # Find the registered URL and runner token for this runner
    url, token = runner_get_url_token

    # Local configuration
    runner_update_local(url, token, local_runner_options)

    # Server configuration
    if property_is_set?(:administration_token)
      update_result = runner_update_server(
                        url,
                        new_resource.http_options,
                        new_resource.administration_token,
                        runner_id,
                        server_runner_options
                      )
      raise "Unable to update runner #{new_resource.name} for url #{new_resource.url}, see error for details." unless update_result
    end
  end
end

action :unregister do
  config_resource_init

  return unless runner_configured_for_host?
  url, token = runner_get_url_token

  if runner_registered?(new_resource.name, url, new_resource.http_options)
    converge_by("Unregister runner #{new_resource.name} from server #{url}.") { runner_unregister!(url, new_resource.http_options, token) }
  end

  if runner_configured_for_host?(new_resource.name, url)
    converge_by("Remove runner #{new_resource.name} from local configuration.") { runner_unconfigure!(token, url) }
  end
end
