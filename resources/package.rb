#
# Cookbook:: gitlab-ci-runner
# Resource:: package
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true

include GitlabCiRunner::Cookbook::Helpers

property :packages, [String, Array],
          default: lazy { default_gitlab_runner_packages },
          coerce: proc { |p| p.is_a?(Array) ? p : [p] }

property :version, String,
          description: 'Package version to install'

property :repository_url, String,
          default: lazy { default_repository_url },
          description: 'Gitlab runner repository URL'

property :repository_options, Hash,
          default: lazy { default_repository_options(repository_url) },
          description: 'Property options to pass the repository resource'

property :package_options, Hash,
          default: {},
          description: 'Property options to pass the pacakge resource'

action_class do
  include GitlabCiRunner::Cookbook::Helpers

  def repo_setup
    case node['platform_family']
    when 'rhel', 'amazon', 'fedora'
      declare_resource(:yum_repository, 'gitlab-ci-runner') do
        new_resource.repository_options.each { |option, value| send(option, value) }
      end
    when 'debian'
      declare_resource(:package, 'lsb-release') do
        action :nothing
      end.run_action(:install)

      declare_resource(:package, 'gitlab-runner supporting packages') do
        package_name %w(apt-transport-https lsb-release)
        action :install
      end

      declare_resource(:apt_repository, 'gitlab-ci-runner') do
        new_resource.repository_options.each { |option, value| send(option, value) }
      end
    else
      raise "#{platform_family} is a unsupported platform family."
    end
  end

  def do_package_action(action)
    declare_resource(:package, 'gitlab-runner') do
      version new_resource.version if new_resource.version
      package_name new_resource.packages

      new_resource.package_options.each { |option, value| send(option, value) }

      action action
    end
  end
end

action :install do
  repo_setup
  do_package_action(action)
end

action :upgrade do
  repo_setup
  do_package_action(action)
end

action :uninstall do
  do_package_action(action)
end
