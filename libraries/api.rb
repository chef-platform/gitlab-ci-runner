#
# Cookbook:: gitlab-ci-runner
# Library:: api
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'net/http'
require 'uri'

require_relative '_common'

module GitlabCiRunner
  module Cookbook
    # Helper module for polling the Gitlab API to configure runners
    module ApiHelpers
      include GitlabCiRunner::Cookbook::CommonHelpers

      API_REQUEST_TYPES ||= %i(get put post delete).freeze
      API_REQUEST_TYPES_DATA ||= %i(put post delete).freeze
      API_USER_AGENT ||= 'gitlab-ci-runner Chef Cookbook'.freeze

      API_BASE_URL ||= 'api/v4'.freeze
      RUNNER ||= "#{API_BASE_URL}/runners".freeze
      RUNNER_USER ||= "#{API_BASE_URL}/user/runners".freeze
      RUNNER_ALL ||= "#{RUNNER}/all".freeze
      RUNNER_VERIFY ||= "#{RUNNER}/verify".freeze

      RUNNER_REGISTER_OPTIONS ||= %w(
        token
        description
        info
        paused
        locked
        run_untagged
        tag_list
        access_level
        maximum_timeout
      ).freeze

      RUNNER_CREATE_OPTIONS ||= %w(
        runner_type
        group_id
        project_id
        description
        paused
        locked
        run_untagged
        tag_list
        access_level
        maximum_timeout
        maintenance_note
      ).freeze

      RUNNER_UPDATE_OPTIONS ||= %w(
        description
        paused
        tag_list
        run_untagged
        locked
        access_level
        maximum_timeout
      ).freeze

      private_constant :API_REQUEST_TYPES, :API_REQUEST_TYPES_DATA, :API_USER_AGENT

      def api_runner_get_all(host, http_options, token)
        api_request(type: :get, url: "#{host}/#{RUNNER_ALL}", options: http_options, token: token)
      end

      def api_runner_get(host, http_options, token, id)
        api_request(type: :get, url: "#{host}/#{RUNNER}/#{id}", options: http_options, token: token)
      end

      def api_runner_token_data(host, http_options, token)
        api_request(type: :post, url: "#{host}/#{RUNNER_VERIFY}", options: http_options, data: { 'token' => token })
      end

      def api_runner_token_valid?(host, http_options, token)
        api_response_is?(api_runner_token_data(host, http_options, token), Net::HTTPOK)
      end

      def api_runner_register(host, http_options, options)
        invalid_options = options.keys - RUNNER_REGISTER_OPTIONS

        unless invalid_options.empty?
          raise ArgumentError,
                "Invalid runner registration options passed: #{invalid_options.join(', ')}"
        end

        api_request(type: :post, url: "#{host}/#{RUNNER}", options: http_options, data: options)
      end

      def api_runner_create(host, http_options, token, options)
        invalid_options = options.keys - RUNNER_CREATE_OPTIONS

        unless invalid_options.empty?
          raise ArgumentError,
                "Invalid runner creation options passed: #{invalid_options.join(', ')}"
        end

        api_request(type: :post, url: "#{host}/#{RUNNER_USER}", options: http_options, token: token, data: options)
      end

      def api_runner_update!(host, http_options, token, id, options)
        invalid_options = options.keys.difference(RUNNER_UPDATE_OPTIONS)

        unless invalid_options.empty?
          raise ArgumentError,
                "Invalid runner update options passed: #{invalid_options.join(', ')}"
        end

        api_request(type: :put, url: "#{host}/#{RUNNER}/#{id}", options: http_options, token: token, data: options)
      end

      def api_runner_remove!(host, http_options, token)
        api_request(type: :delete, url: "#{host}/#{RUNNER}", options: http_options, data: { 'token' => token })
      end

      def api_response_is?(response, type)
        if response.is_a?(type)
          log_chef(:debug, "Response #{response.class} matches expected type #{type}.")
          return true
        end

        log_chef(:error, "Response #{response.class} #{JSON.parse(response.body)} is an error, expected type #{type}.")
        false
      end

      def api_request(type:, url:, options: {}, token: nil, data: nil)
        raise ArgumentError, message: "Invalid API request type #{type}" unless API_REQUEST_TYPES.include?(type)

        log_chef(:debug) do
          log_string = "Performing API #{type.upcase} request to '#{url}'"
          log_string += " token: '#{token}'" unless nil_or_empty?(token)
          log_string += " data: '#{data}'." unless nil_or_empty?(data)

          log_string
        end

        request = request_create(type, url)
        request.set_form_data(data) if API_REQUEST_TYPES_DATA.include?(type)
        request = request_set_headers(request, token)

        response = http_create(url, options).request(request)
        log_chef(:debug) do
          message = "Response #{response.class}, code #{response.code}."
          message << " Body:\n #{JSON.parse(response.body).pretty_inspect}" unless nil_or_empty?(response.body)
          message
        end

        unless response.class < Net::HTTPSuccess
          log_chef(:error, "API Request Error! #{api_error_message_output(response)}")
        end

        response
      end

      def api_error_message_output(response)
        return if response.class < Net::HTTPSuccess

        body = JSON.parse(response.body)

        message = ''
        if body.key?('error')
          message << body.fetch('error')
          message << " - #{body.fetch('error_description')}" if body.key?('error_description')
          message << " Scope: #{body.fetch('scope')}" if body.key?('scope')
        elsif body.key?('message')
          message << body.fetch('message')
        else
          message << 'No message returned from API'
        end

        message
      rescue JSON::ParserError
        log_chef(:warn, 'Failed to parse response as JSON, returning raw response')
        response.body
      end

      def http_create(url, options)
        uri = URI.parse(url)
        log_chef(:info, "Creating HTTP connection for #{uri.host}:#{uri.port} with options #{options}.")
        http = Net::HTTP.new(uri.host, uri.port)

        options.each do |option, value|
          log_chef(:debug, "Applying HTTP option '#{option}' with value '#{value}'.")
          http.send("#{option}=", value)
        end

        if uri.is_a?(URI::HTTPS)
          log_chef(:info, "Using TLS for #{uri.host}:#{uri.port}.")
          http.use_ssl = true
        end

        http
      end

      def request_create(type, url)
        Object.const_get("Net::HTTP::#{type.to_s.capitalize}").send(:new, URI.parse(url).request_uri)
      end

      def request_set_headers(request, token)
        request['User-Agent'] = API_USER_AGENT
        request['Accept'] = 'application/json'
        request['Private-Token'] = token unless token.nil?

        request.each_header { |header, value| log_chef(:debug, "Header: #{header}: #{value}") }

        request
      end
    end
  end
end
