#
# Cookbook:: gitlab-ci-runner
# Library:: _common
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

module GitlabCiRunner
  module Cookbook
    # Common helper methods
    module CommonHelpers
      # Enumerable deep clean proc
      ENUM_DEEP_CLEAN ||= proc do |*args|
        v = args.last
        v.delete_if(&ENUM_DEEP_CLEAN) if v.respond_to?(:delete_if)
        nil_or_empty?(v) && !v.is_a?(String)
      end

      def self.nil_or_empty?(*values)
        values.any? { |v| v.nil? || (v.respond_to?(:empty?) && v.empty?) }
      end

      def nil_or_empty?(*values)
        values.any? { |v| v.nil? || (v.respond_to?(:empty?) && v.empty?) }
      end

      # Call Chef::Log to log a message with the calling method appended
      #
      # @param severity [Symbol] Log severity
      # @param message [String] Log message
      # @yield Lazy loaded log message
      # @yieldreturn [String] Log message
      # @return [nil]
      #
      def log_chef(severity, message = nil)
        severity_int = Mixlib::Log::Logging::LEVELS[severity]
        level = Mixlib::Log::Logging::LEVELS[Chef::Log.level]

        return if severity.nil? || (severity_int < level)

        message = yield if block_given?
        calling_method = caller.find { |v| v.match?(/\.rb:\d+/) }[/`.*'/][1..-2]

        Chef::Log.send(severity, "#{calling_method}: #{message}")
      end

      def compact_hash!(hash)
        hash.delete_if(&ENUM_DEEP_CLEAN)
      end

      def safe_compare(value1, value2)
        v1 = value1.is_a?(Array) ? value1.sort : value1
        v2 = value2.is_a?(Array) ? value2.sort : value2

        v1.eql?(v2)
      end

      def hash_keys_string?(hash)
        raise ArgumentError, "Expected Hash, got #{hash.class}." unless hash.is_a?(Hash)

        return false unless hash.keys.all? { |k| k.is_a?(String) }
        hash.select { |_, v| v.is_a?(Hash) }.each_value { |v| return false unless hash_keys_string?(v) }

        log_chef(:debug, 'Passed: all keys are String class.')
        true
      end
    end
  end
end
