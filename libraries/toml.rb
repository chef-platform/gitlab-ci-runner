#
# Cookbook:: gitlab-ci-runner
# Library:: toml
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'toml-rb'
require_relative '_common'

module GitlabCiRunner
  module Cookbook
    # Helper methods for handling manipulation of config.toml
    module TomlHelpers
      include GitlabCiRunner::Cookbook::CommonHelpers

      def toml_load(file)
        raise "File #{file} does not exist or is not readable" unless ::File.exist?(file) && ::File.readable?(file)

        log_chef(:info, "Loading toml file #{file}.")
        toml = ::TomlRB.load_file(file)
        log_chef(:debug, "Loaded toml file #{file} content:\n#{toml}.")

        toml || {}
      rescue => e
        log_chef(:error, "Failed to read config file: #{file} with error:\n#{e}")

        {}
      end

      def toml_dump(config_hash)
        ::TomlRB.dump(config_hash)
      end
    end
  end
end
