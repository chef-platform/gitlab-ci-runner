#
# Cookbook:: gitlab-ci-runner
# Library:: cookbook_resource
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require_relative '_common'
require_relative 'toml'

module GitlabCiRunner
  module Cookbook
    # Helpers for cookbook resources
    module ResourceHelpers
      include GitlabCiRunner::Cookbook::CommonHelpers
      include GitlabCiRunner::Cookbook::TomlHelpers

      private

      def config_resource_init
        config_resource_create(true) unless config_resource_exist?
      end

      def config_resource_global
        find_resource!(:template, new_resource.config_file).variables['global']
      end

      def config_resource_runners
        find_resource!(:template, new_resource.config_file).variables['runners']
      end

      def config_resource_exist?
        !find_resource!(:template, new_resource.config_file).nil?
      rescue Chef::Exceptions::ResourceNotFound
        false
      end

      def config_resource_create(load_existing)
        with_run_context :root do
          edit_resource(:template, new_resource.config_file) do
            source new_resource.template
            cookbook new_resource.cookbook

            owner new_resource.owner
            group new_resource.group
            mode new_resource.mode
            sensitive new_resource.sensitive

            if load_existing
              log_chef(:info, "Loading existing configuration from #{new_resource.config_file}.")

              variables['global'] = toml_load(new_resource.config_file).select { |k, _| !k.eql?('runners') }
              log_chef(:info, "Loaded #{variables['global'].count} existing global configuration items from #{new_resource.config_file}.")

              variables['runners'] = toml_load(new_resource.config_file).fetch('runners', [])
              log_chef(:info, "Loaded #{variables['runners'].count} existing runner configurations from #{new_resource.config_file}.")
            else
              variables['global'] ||= {}
              variables['runners'] ||= []
            end

            helpers(GitlabCiRunner::Cookbook::CommonHelpers)
            helpers(GitlabCiRunner::Cookbook::TomlHelpers)

            action :nothing
            delayed_action :create
          end
        end
      end
    end
  end
end
