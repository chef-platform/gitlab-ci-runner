#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

gitlab_ci_runner_runner 'test runner c&d' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  registration_token '1234567890'
  # administration_token 'ABC123'
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  options(
    'executor' => 'shell'
  )
  retries 1

  action %i(register update)
end

gitlab_ci_runner_runner 'test runner authentication_token' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  administration_token 'ABC123'
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  tag_list %w(test_tag)
  locked true
  options(
    'limit' => 3,
    'environment' => [
      'PATH=${PATH}:/opt/chefdk/embedded/bin/',
      'BERKSHELF_PATH=/dev/shm/foo/.berkshelf',
    ],
    'executor' => 'shell'
  )
  retries 1

  action :register
end

gitlab_ci_runner_runner 'test_group_authentication_token' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  administration_token 'ABC123'
  runner_type 'group_type'
  group_id 2
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  tag_list %w(test_tag)
  options(
    'limit' => 3,
    'environment' => [
      'PATH=${PATH}:/opt/chefdk/embedded/bin/',
      'BERKSHELF_PATH=/dev/shm/foo/.berkshelf',
    ],
    'executor' => 'shell'
  )
  retries 1

  action :register
end
