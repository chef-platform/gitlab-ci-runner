#
# Copyright:: (c) 2015-2017 Sam4Mobile, 2017-2018 Make.org, 2019 Samuel Bernard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Wait until Gitlab instance is up (max 10 min, for Gitlab shared runner)
require 'net/http'

%w(health readiness liveness).each do |check|
  uri = URI("http://kitchen-gitlab-ce/-/#{check}")

  ruby_block "Waiting for Gitlab #{check}..." do
    block do
      puts "\n"
      code = '-1'
      120.times do |i|
        code = begin
                 Net::HTTP.get_response(uri).code
               rescue StandardError
                 0
               end

        if code == '200'
          puts "    Gitlab #{check} ready, got #{code} after #{i}s"
          break
        end

        puts "    Waiting for Gitlab #{check} to be ready, got #{code}. Try #{i}/120 5s"
        sleep 5
      end
      Chef::Log.warn("Last gitlab status: #{code}") unless code == '200'
    end
  end
end
