#
# Cookbook:: gitlab-ci-runner
# Spec:: api_spec
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

WebMock.disable_net_connect!(allow_localhost: false) # Disable HTTP calls

describe 'GitlabCiRunner::RunnerHelpers' do
  let(:dummy_class) { Class.new { include GitlabCiRunner::Cookbook::ApiHelpers } }

  test_url = 'https://gitlab.test.com'

  body_runners = [
    {
      'active' => true,
      'description' => 'shared-runner-1',
      'id' => 1,
      'ip_address' => '127.0.0.1',
      'is_shared' => true,
      'name' => nil,
      'online' => true,
      'status' => 'online',
    },
    {
      'active' => true,
      'description' => 'shared-runner-2',
      'id' => 3,
      'ip_address' => '127.0.0.1',
      'is_shared' => true,
      'name' => nil,
      'online' => false,
      'status' => 'offline',
    },
    {
      'active' => true,
      'description' => 'test-1-20150125',
      'id' => 6,
      'ip_address' => '127.0.0.1',
      'is_shared' => false,
      'name' => nil,
      'online' => true,
      'status' => 'paused',
    },
  ].to_json

  body_runner = {
    'active' => true,
    'architecture' => nil,
    'description' => 'test-1-20150125',
    'id' => 6,
    'ip_address' => '127.0.0.1',
    'is_shared' => false,
    'contacted_at' => '2016-01-25T16:39:48.066Z',
    'name' => nil,
    'online' => true,
    'status' => 'online',
    'platform' => nil,
    'projects' => [
      {
        'id' => 1,
        'name' => 'GitLab Community Edition',
        'name_with_namespace' => 'GitLab.org / GitLab Community Edition',
        'path' => 'gitlab-foss',
        'path_with_namespace' => 'gitlab-org/gitlab-foss',
      },
    ],
    'token' => '205086a8e3b9a2b818ffac9b89d102',
    'revision' => nil,
    'tag_list' => %w(ruby mysql),
    'version' => nil,
    'access_level' => 'ref_protected',
    'maximum_timeout' => 3600,
  }.to_json

  body_empty = {}.to_json

  before(:each) do
    stub_request(:post, 'https://gitlab.test.com/api/v4/runners/verify')
      .with(
        body: { 'token' => '123' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_empty, headers: {})

    stub_request(:post, 'https://gitlab.test.com/api/v4/runners/verify')
      .with(
        body: { 'token' => '456' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 403, body: body_empty, headers: {})

    stub_request(:get, 'https://gitlab.test.com/api/v4/runners/all')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runners, headers: {})

    stub_request(:get, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:post, 'https://gitlab.test.com/api/v4/runners')
      .with(
        body: { 'token' => '123456789' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(
        status: 201,
        body: '{ "id": "123", "token": "6337ff461c94fd3fa32ba3b1ff4125" }',
        headers: {}
      )

    stub_request(:put, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:put, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        body: { 'tag_list' => "[ 'tag1', 'tag2' ]" },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:delete, 'https://gitlab.test.com/api/v4/runners')
      .with(
        body: { 'token' => 'abc123' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(status: 204, body: body_empty, headers: {})
  end

  describe '.api_runner_get_all' do
    context('given valid runner token') do
      it 'returns Net::HTTPResponse with results' do
        expect(
          dummy_class.new.api_runner_get_all(
            test_url,
            {},
            'admintoken'
          )
        ).to be_a(Net::HTTPOK)
      end
    end
  end

  describe '.api_runner_get' do
    context('given valid runner token') do
      it 'returns Net::HTTPResponse with results' do
        expect(
          dummy_class.new.api_runner_get(
            test_url,
            {},
            'admintoken',
            1
          )
        ).to be_a(Net::HTTPOK)
      end
    end
  end

  describe '.api_runner_token_valid?' do
    context('given valid runner token') do
      it 'returns true' do
        expect(
          dummy_class.new.api_runner_token_valid?(
            test_url,
            {},
            '123'
          )
        ).to eql(true)
      end
    end

    context('given invalid runner') do
      it 'returns false' do
        expect(
          dummy_class.new.api_runner_token_valid?(
            test_url,
            {},
            '456'
          )
        ).to eql(false)
      end
    end
  end

  describe '.api_runner_register' do
    context('given valid registration options') do
      it 'returns Net::HTTPResponse with results' do
        expect(
          dummy_class.new.api_runner_register(
            test_url,
            {},
            'token' => '123456789'
          )
        ).to be_a(Net::HTTPCreated)
      end
    end

    context('given invalid registration options') do
      it 'raises ArgumentError' do
        expect do
          dummy_class.new.api_runner_register(
            test_url,
            {},
            'unsupported' => 'option'
          )
        end.to raise_error(ArgumentError)
      end
    end
  end

  describe '.api_runner_update!' do
    context('given valid update options') do
      it 'returns Net::HTTPOK' do
        expect(
          dummy_class.new.api_runner_update!(
            test_url,
            {},
            'admintoken',
            1,
            'tag_list' => "['mysql', 'ruby' ]"
          )
        ).to be_a(Net::HTTPOK)
      end
    end

    context('given invalid update options') do
      it 'raises ArgumentError' do
        expect do
          dummy_class.new.api_runner_update!(
            test_url,
            {},
            'admintoken',
            1,
            'unsupported' => 'option'
          )
        end.to raise_error(ArgumentError)
      end
    end
  end

  describe '.api_runner_remove!' do
    context('given valid runner token') do
      it 'deletes runner and returns Net::HTTPNoContent' do
        expect(
          dummy_class.new.api_runner_remove!(
            test_url,
            {},
            'abc123'
          )
        ).to be_a(Net::HTTPNoContent)
      end
    end
  end

  describe '.api_request (private)' do
    context('given unsupported request method') do
      it 'raises ArgumentError' do
        expect do
          dummy_class.new.send(
            :api_request,
            type: 'head'.to_sym,
            url: test_url
          )
        end.to raise_error(ArgumentError)
      end
    end
  end
end
