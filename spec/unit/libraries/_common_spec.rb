#
# Cookbook:: gitlab-ci-runner
# Spec:: _common_spec
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

describe 'GitlabCiRunner::CommonHelpers' do
  let(:dummy_class) { Class.new { include GitlabCiRunner::Cookbook::CommonHelpers } }

  describe '.nil_or_empty?' do
    context 'given nil or empty' do
      it 'returns true' do
        expect(dummy_class.new.nil_or_empty?(nil)).to be(true)
      end

      it 'returns true' do
        expect(dummy_class.new.nil_or_empty?([])).to be(true)
      end

      it 'returns true' do
        expect(dummy_class.new.nil_or_empty?('')).to be(true)
      end
    end

    context 'given not nil or empty' do
      it 'returns false' do
        expect(dummy_class.new.nil_or_empty?(['a'])).to be(false)
      end

      it 'returns false' do
        expect(dummy_class.new.nil_or_empty?('a')).to be(false)
      end
    end
  end

  describe '.compact_hash!' do
    hash_with_empty = {
      'a' => 1,
      'b' => {},
      'c' => 3,
    }

    hash_with_empty_result = {
      'a' => 1,
      'c' => 3,
    }

    hash_with_nil = {
      'a' => 1,
      'b' => 2,
      'c' => nil,
    }

    hash_with_nil_result = {
      'a' => 1,
      'b' => 2,
    }

    context 'given hash with empty values' do
      it 'removes them' do
        expect(
          dummy_class.new.compact_hash!(hash_with_empty)
        ).to eql(hash_with_empty_result)
      end
    end

    context 'given hash with nil values' do
      it 'removes them' do
        expect(
          dummy_class.new.compact_hash!(hash_with_nil)
        ).to eql(hash_with_nil_result)
      end
    end
  end
end
