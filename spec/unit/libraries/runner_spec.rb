#
# Cookbook:: gitlab-ci-runner
# Spec:: runner_spec
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

require 'json'
require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: false) # Disable HTTP calls

describe 'GitlabCiRunner::RunnerHelpers' do
  let(:dummy_class) { Class.new { include GitlabCiRunner::Cookbook::RunnerHelpers } }

  test_url = 'https://gitlab.test.com'
  fail_url = 'https://gitlab.fail.com'

  # config = {
  #   'concurrent' => 4,
  #   'check_interval' => 0,
  #   'log_level' => 'info',
  #   'session_server' => {
  #     'session_timeout' => 1800,
  #   },
  #   'runners' => [
  #     {
  #       'name' => '123:Testing Runner',
  #       'url' => test_url,
  #       'token' => 'Ho9KA911jWcTarjsVDti',
  #       'executor' => 'shell',
  #       'cache' => {
  #         's3' => {},
  #         'gcs' => {},
  #       },
  #     },
  #     {
  #       'name' => '456: Testing Docker Runner',
  #       'url' => test_url,
  #       'token' => 'MNzmN6ps5MzGL9zrt4Ce',
  #       'executor' => 'docker',
  #       'docker' => {
  #         'image' => 'centos',
  #         'privileged' => false,
  #         'disable_entrypoint_overwrite' => false,
  #         'oom_kill_disable' => false,
  #         'disable_cache' => false,
  #         'volumes' => [
  #           '/cache',
  #           '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
  #           '/etc/chef:/etc/chef:ro',
  #         ],
  #         'shm_size' => 0,
  #       },
  #       'cache' => {
  #         's3' => {},
  #         'gcs' => {},
  #       },
  #     },
  #   ],
  # }

  # options = {
  #   'url' => test_url,
  #   'registration_token' => 'vEs5WVCUGsKwzEzHESED',
  #   'executor' => 'docker',
  #   'docker' => {
  #     'image' => 'centos',
  #     'privileged' => false,
  #     'disable_entrypoint_overwrite' => false,
  #     'oom_kill_disable' => false,
  #     'disable_cache' => false,
  #     'volumes' => [
  #       '/cache',
  #       '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
  #       '/etc/chef:/etc/chef:ro',
  #     ],
  #     'shm_size' => 0,
  #   },
  #   'cache' => {
  #     's3' => {},
  #     'gcs' => {},
  #   },
  # }

  # config_changed = {
  #   'concurrent' => 4,
  #   'check_interval' => 0,
  #   'log_level' => 'info',
  #   'session_server' => {
  #     'session_timeout' => 1800,
  #   },
  #   'runners' => [
  #     {
  #       'name' => '123:Testing Runner',
  #       'url' => test_url,
  #       'token' => 'Ho9KA911jWcTarjsVDti',
  #       'executor' => 'shell',
  #       'cache' => {
  #         's3' => {},
  #         'gcs' => {},
  #       },
  #     },
  #     {
  #       'name' => '456: Testing Docker Runner',
  #       'url' => test_url,
  #       'token' => 'MNzmN6ps5MzGL9zrt4Ce',
  #       'executor' => 'docker',
  #       'docker' => {
  #         'image' => 'centos',
  #         'privileged' => true,
  #         'disable_entrypoint_overwrite' => false,
  #         'oom_kill_disable' => false,
  #         'disable_cache' => false,
  #         'volumes' => [
  #           '/cache',
  #           '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
  #           '/etc/chef:/etc/chef:ro',
  #         ],
  #         'shm_size' => 0,
  #       },
  #       'cache' => {
  #         's3' => {
  #           'ServerAddress' => 's3.amazonaws.com',
  #           'AccessKey' => 'AMAZON_S3_ACCESS_KEY',
  #           'SecretKey' => 'AMAZON_S3_SECRET_KEY',
  #           'BucketName' => 'runners-cache',
  #           'BucketLocation' => 'eu-west-1',
  #           'Insecure' => false,
  #         },
  #         'gcs' => {},
  #       },
  #     },
  #   ],
  # }

  # options_changed = {
  #   'url' => test_url,
  #   'registration_token' => 'vEs5WVCUGsKwzEzHESED',
  #   'executor' => 'docker',
  #   'docker' => {
  #     'image' => 'centos',
  #     'privileged' => true,
  #     'disable_entrypoint_overwrite' => false,
  #     'oom_kill_disable' => false,
  #     'disable_cache' => false,
  #     'volumes' => [
  #       '/cache',
  #       '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
  #       '/etc/chef:/etc/chef:ro',
  #     ],
  #     'shm_size' => 0,
  #   },
  #   'cache' => {
  #     's3' => {
  #       'ServerAddress' => 's3.amazonaws.com',
  #       'AccessKey' => 'AMAZON_S3_ACCESS_KEY',
  #       'SecretKey' => 'AMAZON_S3_SECRET_KEY',
  #       'BucketName' => 'runners-cache',
  #       'BucketLocation' => 'eu-west-1',
  #       'Insecure' => false,
  #     },
  #     'gcs' => {},
  #   },
  # }

  body_runners = [
    {
      'active' => true,
      'description' => 'shared-runner-1',
      'id' => 1,
      'ip_address' => '127.0.0.1',
      'is_shared' => true,
      'name' => nil,
      'online' => true,
      'status' => 'online',
    },
    {
      'active' => true,
      'description' => 'shared-runner-2',
      'id' => 3,
      'ip_address' => '127.0.0.1',
      'is_shared' => true,
      'name' => nil,
      'online' => false,
      'status' => 'offline',
    },
    {
      'active' => true,
      'description' => 'test-1-20150125',
      'id' => 6,
      'ip_address' => '127.0.0.1',
      'is_shared' => false,
      'name' => nil,
      'online' => true,
      'status' => 'paused',
    },
  ].to_json

  body_runner = {
    'active' => true,
    'architecture' => nil,
    'description' => 'test-1-20150125',
    'id' => 6,
    'ip_address' => '127.0.0.1',
    'is_shared' => false,
    'contacted_at' => '2016-01-25T16:39:48.066Z',
    'name' => nil,
    'online' => true,
    'status' => 'online',
    'platform' => nil,
    'projects' => [
      {
        'id' => 1,
        'name' => 'GitLab Community Edition',
        'name_with_namespace' => 'GitLab.org / GitLab Community Edition',
        'path' => 'gitlab-foss',
        'path_with_namespace' => 'gitlab-org/gitlab-foss',
      },
    ],
    'token' => '205086a8e3b9a2b818ffac9b89d102',
    'revision' => nil,
    'tag_list' => %w(tag1 tag2),
    'version' => nil,
    'access_level' => 'ref_protected',
    'maximum_timeout' => 3600,
  }.to_json

  body_empty = {}.to_json

  before(:each) do
    stub_request(:post, 'https://gitlab.test.com/api/v4/runners/verify')
      .with(
        body: { 'token' => '123' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_empty, headers: {})

    stub_request(:post, 'https://gitlab.test.com/api/v4/runners/verify')
      .with(
        body: { 'token' => '456' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 403, body: body_empty, headers: {})

    stub_request(:get, 'https://gitlab.test.com/api/v4/runners/all')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runners, headers: {})

    stub_request(:get, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:get, 'https://gitlab.fail.com/api/v4/runners/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 500, body: body_runner, headers: {})

    stub_request(:post, 'https://gitlab.test.com/api/v4/runners')
      .with(
        body: { 'token' => '123456789' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(
        status: 201,
        body: '{ "id": "123", "token": "6337ff461c94fd3fa32ba3b1ff4125" }',
        headers: {}
      )

    stub_request(:post, 'https://gitlab.fail.com/api/v4/runners')
      .with(
        body: { 'token' => '123456789' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(
        status: 403,
        body: body_empty,
        headers: {}
      )

    stub_request(:put, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:put, 'https://gitlab.test.com/api/v4/runners/1')
      .with(
        body: { 'tag_list' => "[ 'tag1', 'tag2' ]" },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 200, body: body_runner, headers: {})

    stub_request(:put, 'https://gitlab.fail.com/api/v4/runners/1')
      .with(
        body: { 'tag_list' => "[ 'tag1', 'tag2' ]" },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'Private-Token' => 'admintoken',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      ).to_return(status: 500, body: body_empty, headers: {})

    stub_request(:delete, 'https://gitlab.test.com/api/v4/runners')
      .with(
        body: { 'token' => 'abc123' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(status: 204, body: body_empty, headers: {})

    stub_request(:delete, 'https://gitlab.fail.com/api/v4/runners')
      .with(
        body: { 'token' => '123456789' },
        headers: {
          'Accept' => 'application/json',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Content-Type' => 'application/x-www-form-urlencoded',
          'User-Agent' => 'gitlab-ci-runner Chef Cookbook',
        }
      )
      .to_return(status: 500, body: body_empty, headers: {})
  end

  describe '.runner_options_to_h' do
    config_hash = {
      'url' => test_url,
      'registration_token' => '1234567890',
      'executor' => 'docker',
      'docker' => {
        'image' => 'centos',
        'privileged' => false,
        'disable_entrypoint_overwrite' => false,
        'oom_kill_disable' => false,
        'disable_cache' => false,
        'volumes' => [
          '/cache',
          '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
          '/etc/chef:/etc/chef:ro',
        ],
        'shm_size' => 0,
      },
      'cache' => {
        's3' => {},
        'gcs' => {},
      },
    }

    config_string = {
      'url' => test_url,
      'registration_token' => '1234567890',
      'executor' => 'docker',
      'docker_image' => 'centos',
      'docker_privileged' => false,
      'docker_disable_entrypoint_overwrite' => false,
      'docker_oom_kill_disable' => false,
      'docker_disable_cache' => false,
      'docker_volumes' => [
        '/cache',
        '/home/gitlab-runner/.chef:/home/gitlab-runner/.chef:ro',
        '/etc/chef:/etc/chef:ro',
      ],
      'docker_shm_size' => 0,
      'cache' => {
        's3' => {},
        'gcs' => {},
      },
    }

    context 'given runner config as hash' do
      it 'returns same config hash' do
        expect(
          dummy_class.new.runner_options_to_h(config_hash)
        ).to be_a(Hash)
        expect(
          dummy_class.new.runner_options_to_h(config_hash)
        ).to eql(config_hash)
      end
    end

    context 'given runner config as string' do
      it 'returns config converted to hash' do
        expect(
          dummy_class.new.runner_options_to_h(config_string)
        ).to be_a(Hash)
        expect(
          dummy_class.new.runner_options_to_h(config_string)
        ).to eql(config_hash)
      end
    end
  end

  describe '.runner_register' do
    before(:each) do
      allow(File).to receive(:file?).and_call_original
      allow(TomlRB).to receive(:load_file).and_call_original
    end

    context 'given runner to register' do
      it 'registers runner' do
        expect(
          dummy_class.new.runner_register(
            test_url,
            {},
            '123456789',
            {}
          )
        ).to be_a(Array)
        expect(
          dummy_class.new.runner_register(
            test_url,
            {},
            '123456789',
            {}
          )
        ).to eql(%w(123 6337ff461c94fd3fa32ba3b1ff4125))
      end
    end

    context 'given register error' do
      it 'raises RuntimeError' do
        expect do
          dummy_class.new.runner_register(
            fail_url,
            {},
            '123456789',
            {}
          )
        end.to raise_error(RuntimeError)
        expect do
          dummy_class.new.runner_register(
            fail_url,
            {},
            '123456789',
            {}
          )
        end.to raise_error(RuntimeError)
      end
    end

    context 'given no registration token' do
      it 'raises ArgumentError' do
        expect do
          dummy_class.new.runner_register(
            test_url,
            {},
            '',
            {}
          )
        end.to raise_error(ArgumentError)
        expect do
          dummy_class.new.runner_register(
            test_url,
            {},
            nil,
            {}
          )
        end.to raise_error(ArgumentError)
      end
    end
  end

  describe '.runner_unregister!' do
    before(:each) do
      allow(File).to receive(:file?).and_call_original
      allow(TomlRB).to receive(:load_file).and_call_original
    end

    context 'given runner to unregister' do
      it 'returns true' do
        expect(
          dummy_class.new.runner_unregister!(
            test_url,
            {},
            'abc123'
          )
        ).to be(true)
      end
    end

    context 'given unregister error' do
      it 'returns false' do
        expect(
          dummy_class.new.runner_unregister!(
            fail_url,
            {},
            '123456789'
          )
        ).to be(false)
      end
    end
  end

  describe '.runner_update_server' do
    before(:each) do
      allow(File).to receive(:file?).and_call_original
      allow(TomlRB).to receive(:load_file).and_call_original
    end

    context 'given runner update success' do
      it 'returns true' do
        expect(
          dummy_class.new.runner_update_server(
            test_url,
            {},
            'admintoken',
            1,
            'tag_list' => "[ 'tag1', 'tag2' ]"
          )
        ).to be(true)
      end
    end

    context 'given runner update fail' do
      it 'returns false' do
        expect(
          dummy_class.new.runner_update_server(
            fail_url,
            {},
            'admintoken',
            1,
            'tag_list' => "[ 'tag1', 'tag2' ]"
          )
        ).to be(false)
      end
    end
  end
end
