require 'chefspec'
require 'chefspec/berkshelf'

require 'rexml/rexml'
require 'rexml/parseexception'

require 'webmock/rspec'

require 'json'

# Require all our libraries
Dir['libraries/*.rb'].each { |f| require File.expand_path(f) }
