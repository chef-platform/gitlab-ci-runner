# Gitlab CI Runner

## Description

Install and configure **gitlab-ci-runner**. Learn more at https://docs.gitlab.com/runner.

## Change Log

- See [CHANGELOG.md](/CHANGELOG.md) for version details and changes.

## Requirements
### Cookbooks

- None

### Gems

- `toml-rb`

## Platforms

The following platforms are supported and tested with Test Kitchen:

- RHEL/CentOS 7+
- Debian 8+

## Resources

The following resources are provided:

- [gitlab_ci_runner_global_config](documentation/gitlab_ci_runner_global_config.md)
- [gitlab_ci_runner_package](documentation/gitlab_ci_runner_package.md)
- [gitlab_ci_runner_service](documentation/gitlab_ci_runner_service.md)
- [gitlab_ci_runner_runner](documentation/gitlab_ci_runner_runner.md)

## Testing

This cookbook is tested through the installation of the full gitlab platform in docker hosts using kitchen-dokken.

- kitchen-gitlab-gitlab-ce-kitchen: Complete instance of **gitlab-ce**
- kitchen-gitlab-ci-runner-*os*: Installation and registration of gitlab-ci-runner for the relevant operating system.

For more information, see [kitchen.yml](kitchen.yml) and [test](test) directory.

## Contributing

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge request.

## License and Author

- Original Author:: Samuel Bernard (<samuel.bernard@gmail.com>)
- Major refactor and current maintainer:: Ben Hughes (<bmhughes@bmhughes.co.uk>)

```text
Copyright (c) 2015-2017 Sam4Mobile, 2017-2018 Make.org, 2019 Samuel Bernard

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
