name 'gitlab-ci-runner'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform-gitlab-ci-runner-2784918-issue-@incoming.gitlab.com'
license 'Apache-2.0'
description 'Installs/Configures Gitlab CI Runner'
source_url 'https://gitlab.com/chef-platform/gitlab-ci-runner'
issues_url 'https://gitlab.com/chef-platform/gitlab-ci-runner/issues'
version '4.2.0'

chef_version '>= 14'

%w(redhat centos fedora debian ubuntu).each { |os| supports os }

gem 'toml-rb', '~> 2.2'
